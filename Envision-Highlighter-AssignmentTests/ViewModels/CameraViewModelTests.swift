//
//  CameraViewModel.swift
//  Envision-Highlighter-AssignmentTests
//
//  Created by Hoye Lam on 01/02/2021.
//

import XCTest
@testable import Envision_Highlighter_Assignment

class CameraViewModelTests: XCTestCase {
    
    func test_perform_orc() {
        // Given
        let sut = CameraViewModel()
        let snapshot = UIImage(named: "snapshot")!
        let expectedHighlightedText = "mail on sunday"
        let ocrExpectation = expectation(description: "OCR Performed on Snapshot")
        
        // When
        
        /// OCR Automatically performed by view model
        sut.image = snapshot
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            ocrExpectation.fulfill()
        }
        
        // Then
        waitForExpectations(timeout: 1.0) { (error) in
            XCTAssert(sut.previewHighlight.lowercased().contains(expectedHighlightedText))
        }
    }
}
